import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { contacts } from "./contacts";
import { settings } from "./settings";
import { reducer as formReducer } from 'redux-form';

export default combineReducers ({
	contacts,
    settings,
	router: routerReducer,
    form: formReducer
});