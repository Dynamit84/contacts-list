import {
	CONTACTS_FETCH_SUCCEEDED,
	CONTACT_REMOVE_SUCCEEDED,
	CONTACT_ADD_SUCCEEDED,
	CONTACT_EDIT_SUCCEEDED
} from "../constants";
import { handleActions } from 'redux-actions';

export const contacts = handleActions({
    [CONTACTS_FETCH_SUCCEEDED]: (state, { payload }) => ([
        ...state,
        ...payload
	]),
    [CONTACT_REMOVE_SUCCEEDED]: (state, { payload }) => {
		return state.filter(contact => contact.id !== payload);
	},
    [CONTACT_ADD_SUCCEEDED]: (state, { payload }) => ([
        ...state,
        payload
    ]),
    [CONTACT_EDIT_SUCCEEDED]: (state, { payload }) => {
		const contactEditIndex = state.findIndex(contact => contact.id === payload.id);
		return [
			...state.slice(0, contactEditIndex),
			payload,
			...state.slice(contactEditIndex + 1)
		];
	},
}, []);