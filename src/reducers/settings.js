import {
    CONTACTS_UPDATE_SETTINGS
} from "../constants";
import { handleActions } from 'redux-actions';

const initialState = {
    pageSize: 10
};

export const settings = handleActions({
    [CONTACTS_UPDATE_SETTINGS]: (state, { payload }) => ({
        ...state,
        ...payload
    })
}, initialState);