import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { logger } from 'redux-logger';
import {createBrowserHistory} from 'history';
import { routerMiddleware } from 'react-router-redux';
import reducer from './reducers/reducers';
import rootSaga from './sagas/rootSaga';

export const history = createBrowserHistory();
const middleware = routerMiddleware(history);
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
	reducer,
	applyMiddleware(middleware, sagaMiddleware, logger)
);

sagaMiddleware.run(rootSaga);

export default store;
