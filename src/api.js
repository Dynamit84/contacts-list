export const fetchContacts = async () => {
	try {
		const response = await fetch('http://localhost:5000/contacts');
		
		return await response.json();
	} catch (e) {
		console.log(e);
	}
};

export const removeContact = async (id) => {
	try {
		const response = await fetch(`http://localhost:5000/contacts/${id}`, {
			method: 'DELETE'
		});
		
		return await response.json();
	} catch (e) {
		console.log(e);
	}
};

export const addContact = async (data) => {
    try {
        const response = await fetch('http://localhost:5000/contacts', {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
			body: data
        });

        return await response.json();
    } catch (e) {
        console.log(e);
    }
};

export const editContact = async (data, id) => {
	try {
		const response = await fetch(`http://localhost:5000/contacts/${id}`, {
			method: 'PUT',
			headers: new Headers({
				'Content-Type': 'application/json'
			}),
			body: data
		});
		
		return await response.json();
	} catch (e) {
		console.log(e);
	}
};