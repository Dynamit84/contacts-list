import React, { Component, Fragment } from 'react';
import { Header1 } from "./styled-components/StyledComponents";
import SettingsForm from './Forms/SettingsForm';
import { connect } from 'react-redux';
import { contactsUpdateSettings } from '../actions/actions';

class Settings extends Component {

    submit = values => {
    	this.props.contactsUpdateSettings(values);
        this.props.history.push('/home');
    };
	
	render() {
		return (
			<Fragment>
				<Header1>Contacts Settings</Header1>
				<SettingsForm onSubmit={this.submit} />
			</Fragment>
		);
	}
}

export default connect(null, {contactsUpdateSettings})(Settings);