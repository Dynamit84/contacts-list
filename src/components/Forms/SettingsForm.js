import React, { Component, Fragment } from 'react';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Slider } from 'redux-form-material-ui'

const StyledPaper = styled(Paper)`
	max-width: 600px;
	margin: 30px auto;
	padding: 30px;
    text-align: center;
`;

class SettingsForm extends Component{

   render() {
       const { handleSubmit, pristine, submitting, valid, pageSize } = this.props;
       return (
           <Fragment>
               <StyledPaper zDepth={2}>
                   <form onSubmit={handleSubmit}>
                       <Field name="pageSize"
                              component={Slider}
                              min={5}
                              max={50}
                              step={1}
                              defaultValue={pageSize}
                       />
                       <p>
                           <span>{'New page size is: '}</span>
                           <span>{pageSize}</span>
                       </p>
                       <RaisedButton style={{marginTop: '20px'}}
                                     label="Submit"
                                     primary={true}
                                     type="submit"
                                     disabled={pristine || submitting || !valid}/>
                   </form>
               </StyledPaper>
           </Fragment>
       )
   }
}

SettingsForm = reduxForm({
    form: 'settings'
})(SettingsForm);

const selector = formValueSelector('settings');

SettingsForm = connect(
    state => {
        const pageSize = selector(state, 'pageSize');
        return {
            initialValues: state.settings,
            pageSize
        }
    }
)(SettingsForm);

export default SettingsForm;