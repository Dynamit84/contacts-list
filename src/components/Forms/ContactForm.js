import React, { Fragment } from 'react';
import { Field, reduxForm } from 'redux-form';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import { Header1 } from "../styled-components/StyledComponents";
import styled from 'styled-components';

const validate = values => {
	const errors = {};
	const requiredFields = [
		'name',
		'phone',
		'email'
	];
	requiredFields.forEach(field => {
		if (!values[field]) {
			errors[field] = 'Required'
		}
	});
	if (
		values.email &&
		!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
	) {
		errors.email = 'Invalid email address'
	}
	return errors;
};

const renderTextField = ({
	 input,
	 label,
	 meta: { touched, error },
	 ...custom
	}) => (
	<TextField
		hintText={label}
		floatingLabelText={label}
		errorText={touched && error}
		{...input}
		{...custom}
	/>
);

const StyledPaper = styled(Paper)`
	max-width: 600px;
	margin: 30px auto;
	padding: 30px;
    text-align: center;
`;

let ContactForm = props => {
    const { handleSubmit, pristine, submitting, valid, pathname } = props;
    return (
        <Fragment>
            <Header1>{pathname.includes('edit') ? 'Edit' : 'Add'}  Contact</Header1>
            <StyledPaper zDepth={2}>
                <form onSubmit={handleSubmit}>
                    <div>
                        <Field name="name"
                               component={renderTextField}
                               style={{width: '80%'}}
                               label="Full Name" />
                    </div>
                    <div>
                        <Field name="phone"
                               component={renderTextField}
                               style={{width: '80%'}}
                               label="Phone number" />
                    </div>
                    <div>
                        <Field name="email"
                               component={renderTextField}
                               style={{width: '80%'}}
                               label="Email" />
                    </div>
                    <RaisedButton style={{marginTop: '20px'}}
                                  label="Submit"
                                  primary={true}
                                  type="submit"
                                  disabled={pristine || submitting || !valid}/>
                </form>
            </StyledPaper>
		</Fragment>
    )
};

ContactForm = reduxForm({
    form: 'contact',
	validate
})(ContactForm);

export default ContactForm;