import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import person from '../assets/img/person.png';
import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
import {grey400} from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';

export const Contact = props => {

	const { name, id, email, phone, onRemoveClick, onViewClick, onEditClick, isLast } = props;
	
	const iconButtonElement = (
		<IconButton
			touch={true}
			tooltip="more"
			tooltipPosition="bottom-left"
			style={{top: '18px'}}
		>
			<MoreVertIcon color={grey400} />
		</IconButton>
	);
	
	const rightIconMenu = (
		<IconMenu iconButtonElement={iconButtonElement}>
			<MenuItem onClick={() => onViewClick(id)}>View</MenuItem>
			<MenuItem onClick={() => onEditClick(id)}>Edit</MenuItem>
			<MenuItem onClick={() => onRemoveClick(id)}>Remove</MenuItem>
		</IconMenu>
	);
	
	return (
		<Fragment>
				<ListItem
					leftAvatar={<NavLink to={`contacts/${id}`}><Avatar src={person}/></NavLink>}
					rightIconButton={rightIconMenu}
					primaryText={<NavLink to={`contacts/${id}`}>{name}</NavLink>}
					secondaryText={
						<NavLink to={`contacts/${id}`}>
							<div>
								<span style={{'display': 'block'}}>{phone}</span>
								<span>{email}</span>
							</div>
						</NavLink>
					}
					secondaryTextLines={2}
				/>
			{
				!isLast && <Divider inset={true} />
			}
		</Fragment>
		
	);
};