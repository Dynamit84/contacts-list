import React, { Fragment } from 'react';
import {
	Table,
	TableBody,
	TableHeader,
	TableHeaderColumn,
	TableRow,
	TableRowColumn,
} from 'material-ui/Table';
import styled from 'styled-components';

const TableContainer = styled.div`
	max-width: 700px;
	margin: 0 auto;
	outline: 1px solid #D3D3D3;
`;

const Title2 = styled.h2`
	text-align: center;
`;

const StyledTableRow = styled(TableRow)`
	background-color: ${props => props.type === 'incoming' ? '#9fdc9f' : '#f9d66e'};
`;

const HistoryCallsTable = props => {
	const { callsHistory } = props;
	
	if(!callsHistory.length) {
		return null;
	}
	
	return (
		<Fragment>
			<Title2>Calls history</Title2>
			<TableContainer>
				<Table>
					<TableHeader style={{backgroundColor: 'rgb(236, 236, 236)'}}
								 displaySelectAll={false}
								 adjustForCheckbox={false}>
						<TableRow>
							<TableHeaderColumn>Name</TableHeaderColumn>
							<TableHeaderColumn>Phone</TableHeaderColumn>
							<TableHeaderColumn>Status</TableHeaderColumn>
						</TableRow>
					</TableHeader>
					<TableBody displayRowCheckbox={false}>
						{
							callsHistory.map(call => {
								const { name, phone, isIncoming, id } = call;
								const callType = isIncoming ? 'incoming': 'outgoing';
								return (
									<StyledTableRow type={callType} key={id}>
										<TableRowColumn>{name}</TableRowColumn>
										<TableRowColumn>{phone}</TableRowColumn>
										<TableRowColumn>{callType}</TableRowColumn>
									</StyledTableRow>
								);
							})
						}
					</TableBody>
				</Table>
			</TableContainer>
		</Fragment>
	);
};

export default HistoryCallsTable;