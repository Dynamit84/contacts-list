import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FlatButton from 'material-ui/FlatButton';
import styled from 'styled-components';
import NavigationChevronLeft from 'material-ui/svg-icons/navigation/chevron-left';
import NavigationChevronRight from 'material-ui/svg-icons/navigation/chevron-right';
import NavigationLastPage from 'material-ui/svg-icons/navigation/last-page';
import NavigationFirstPage from 'material-ui/svg-icons/navigation/first-page';

const PagesContainer = styled.ul`
	display: flex;
	justify-content: center;
`;

const buttonStyle = {
	minWidth: '50px'
};

class Pagination extends Component {
	constructor() {
		super();
		this.state = { pager: {} };
	}
	
	componentWillMount() {
		if (this.props.contacts && this.props.contacts.length) {
			this.setPage(this.props.initialPage);
		}
	}
	
	componentDidUpdate(prevProps) {
		if (this.props.contacts !== prevProps.contacts) {
			this.setPage(this.props.initialPage);
		}
	}
	
	setPage(page) {
		const { contacts, onChangePageClick, pageSize } = this.props;
		let { pager } = this.state;
		
		if (page < 1 || page > pager.totalPages) {
			return;
		}
		
		pager = this.getPager(contacts.length, page, pageSize);
		
		const contactsToRender = contacts.slice(pager.startIndex, pager.endIndex + 1);
		
		this.setState({ pager: pager });
		
		onChangePageClick(contactsToRender);
	}
	
	getPager(totalItems, currentPage = 1, pageSize = 10) {
		const totalPages = Math.ceil(totalItems / pageSize);
		let startPage, lastPage;
		
		switch (true) {
			case (totalPages <= 10):
				startPage = 1;
				lastPage = totalPages;
				break;
			case (currentPage <= 6 && totalPages > 10):
				startPage = 1;
				lastPage = 10;
				break;
			case (currentPage + 4 >= totalPages): {
				startPage = totalPages - 9;
				lastPage = totalPages;
				break;
			}
			default:
				startPage = currentPage - 5;
				lastPage = currentPage + 4;
			
		}
		
		const startIndex = (currentPage - 1) * pageSize;
		const endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
		let pages;
		if(currentPage >= Math.min(totalPages/2)) {
			pages = Array.from(Array(totalPages), (e,i)=>i+1).slice(-10);
		} else {
			pages = Array.from(Array(totalPages), (e,i)=>i).slice(currentPage, 10 + currentPage);
		}
		
		return {
			totalItems,
			currentPage,
			pageSize,
			totalPages,
			startPage,
			lastPage,
			startIndex,
			endIndex,
			pages
		};
	}
	
	render() {
		const { pager } = this.state;
		
		if (!pager.pages || pager.pages.length <= 1) {
			return null;
		}
		
		return (
			<PagesContainer>
				<li>
					<FlatButton icon={<NavigationFirstPage/>}
								onClick={() => this.setPage(1)}
								disabled={pager.currentPage === 1}
								style={buttonStyle}/>
				</li>
				<li className={pager.currentPage === 1 ? 'disabled' : ''}>
					<FlatButton icon={<NavigationChevronLeft/>}
								onClick={() => this.setPage(pager.currentPage - 1)}
								disabled={pager.currentPage === 1}
								style={buttonStyle}/>
				</li>
				{pager.pages.map((page, index) =>
					<li key={index} className={pager.currentPage === page ? 'active' : ''}>
						<FlatButton label={page}
									onClick={() => this.setPage(page)}
									backgroundColor={pager.currentPage === page ? 'rgb(0, 188, 212)': ''}
									style={buttonStyle}/>
					</li>
				)}
				<li>
					<FlatButton icon={<NavigationChevronRight/>}
								onClick={() => this.setPage(pager.currentPage + 1)}
								disabled={pager.currentPage === pager.totalPages}
								style={buttonStyle}/>
				</li>
				<li>
					<FlatButton icon={<NavigationLastPage/>}
								onClick={() => this.setPage(pager.totalPages)}
								disabled={pager.currentPage === pager.totalPages}
								style={buttonStyle}/>
				</li>
			</PagesContainer>
		);
	}
}

const propTypes = {
	contacts: PropTypes.array.isRequired,
	onChangePageClick: PropTypes.func.isRequired,
	initialPage: PropTypes.number
};

const defaultProps = {
	initialPage: 1
};

Pagination.propTypes = propTypes;
Pagination.defaultProps = defaultProps;

export default Pagination;