import React, { Component, Fragment } from 'react';
import { connect} from 'react-redux';
import { compose } from 'redux';
import  { Contact } from './Contact';
import { contactRemoveRequested } from "../actions/actions";
import { List } from 'material-ui/List';
import Pagination from "./Pagination";
import PropTypes from "prop-types";
import { Loader } from "./Loader";

const listStyle = {
	padding: 0,
	maxWidth: '500px',
	margin: '30px auto',
	outline: '1px solid #D3D3D3'
};

class Home extends Component {
	constructor() {
		super();
		this.state = {
			pageContacts: []
		}
	}
	
	onChangePage(contacts) {
		this.setState({
			pageContacts: contacts
		});
	}
	
	onRemoveContact(id) {
		this.props.removeContact(id);
	}

	onViewContact(id) {
        this.props.history.push(`contacts/${id}`);
	}
	
	onEditContact(id){
		this.props.history.push(`edit/${id}`);
	}
	
	render() {
		const { contacts, pageSize } = this.props;
		return (
			<Fragment>
				<Pagination contacts={contacts}
							pageSize={pageSize}
							onChangePageClick={this.onChangePage.bind(this)}/>
				<List style={listStyle}>
					{
						this.state.pageContacts.map((contact, index, arr) =>
							<Contact key={contact.id} { ...contact }
									 onRemoveClick={this.onRemoveContact.bind(this)}
									 onViewClick={this.onViewContact.bind(this)}
									 onEditClick={this.onEditContact.bind(this)}
									 isLast={index + 1 === arr.length}/>)
					}
				</List>
			</Fragment>
		);
	}
}

const mapStateToProps = (state) => {
	
	return {
		contacts: state.contacts,
		pageSize: state.settings.pageSize
	}
};

const propTypes = {
    contacts: PropTypes.array.isRequired,
    removeContact: PropTypes.func.isRequired,
};

Home.propTypes = propTypes;

export default compose(connect(mapStateToProps, { removeContact: contactRemoveRequested })/*, Loader('contacts')*/)(Home);

