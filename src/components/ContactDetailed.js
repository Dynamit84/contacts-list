import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter} from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import HistoryCallsTable from './HistoryCallsTable';
import styled from 'styled-components';
import Avatar from 'material-ui/Avatar';
import person from '../assets/img/person.png';
import { contactRemoveRequested } from '../actions/actions';
import PropTypes from 'prop-types';
import makeGetContact from '../selectors/selectors';

const PageContainer = styled.div`
	text-align: center;
`;

const InfoContainer = styled.div`
	display: flex;
	align-items: center;
    justify-content: center;
    text-align: left;
    margin: 30px 0 10px;
`;

const ButtonsContainer = styled.div`
	display: flex;
	justify-content: space-around;
	max-width: 220px;
	margin: 0 auto;
`;

const Name = styled.p`
	font-weight: bold;
	font-size: 20px;
`;

class ContactDetailed extends Component {
	
	render() {
		
		const { contact, history, removeContact } = this.props;
		if(!contact) {
			return null;
		}
		const { name, id, email, phone, callsHistory } = contact;
		
		return (
			<PageContainer>
				<InfoContainer>
					<Avatar size={100} src={person}/>
					<div style={{marginLeft: '20px'}}>
						<Name>{name}</Name>
						<p>{phone}</p>
						<p>{email}</p>
					</div>
				</InfoContainer>
				<ButtonsContainer>
					<RaisedButton label="edit"
								  primary={true}
								  onClick={() => history.push(`/edit/${id}`)}/>
					<RaisedButton label="remove"
								  secondary={true}
								  onClick={() => removeContact(id)}/>
				</ButtonsContainer>
				<HistoryCallsTable callsHistory={callsHistory}/>
			</PageContainer>
		);
	}
}

const mapStateToProps = () => {
    const getContact = makeGetContact();

    return (state, ownProps) => {
        return {
            contact: getContact(state, ownProps)
        }
    };
};

const propTypes = {
    contact: PropTypes.object.isRequired,
    removeContact: PropTypes.func.isRequired,
	history: PropTypes.object.isRequired
};

ContactDetailed.propTypes = propTypes;

export default compose(withRouter, connect(mapStateToProps, { removeContact: contactRemoveRequested }))(ContactDetailed);