import React, { Component } from 'react';
import RefreshIndicator from 'material-ui/RefreshIndicator';

export const Loader = (propName) => (WrappedComponent) => {
    return class LoaderHOC extends Component {
        isEmpty(prop) {
            return (
                prop === null
                || prop === undefined
                || (Array.isArray(prop) && prop.length === 0)
                || (prop.constructor === Object && Object.keys(prop).length === 0)
            );
        }
        render() {
            return (
                this.isEmpty(this.props[propName])
                    ? <RefreshIndicator
                        size={50}
                        left={70}
                        top={0}
                        loadingColor="#FF9800"
                        status="loading"/>
                    : <WrappedComponent {...this.props} />)
        }
    }
};