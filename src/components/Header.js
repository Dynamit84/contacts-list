import React from 'react';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import TextField from 'material-ui/TextField';

const titleStyle = {
	flexGrow: 0,
	flexShrink: 0,
	flexBasis: '15%'
};

const appBarStyle = {
    alignItems: 'center'
};

const buttonStyle = {
	color: '#fff'
};

const inputStyle = {
    color: '#fff',
    padding: '0 10px'
};

const underlineFocusStyle = {
	borderColor: '#2196f3'
};

const hintStyle = {
	padding: '0 10px'
};

const Nav = styled.nav`
	display: flex;
	align-items: center;
	flex-grow: 2; 		
`;

export const Header = (props) => {
	const isHomePage = props.location.pathname.includes('home');
	return (
		<AppBar title='Contacts'
				titleStyle={titleStyle}
				style={appBarStyle}
				showMenuIconButton={false}>
            <Nav>
                <FlatButton style={buttonStyle} label="home" containerElement={<Link to="/home" />}/>
                <FlatButton style={buttonStyle} label="add contact" containerElement={<Link to="/create" />}/>
				<FlatButton style={buttonStyle} label="settings" containerElement={<Link to="/settings" />}/>
			</Nav>
			{
				isHomePage && (
                    <TextField
                        hintText="Search Contact"
                        inputStyle={inputStyle}
                        underlineFocusStyle={underlineFocusStyle}
                        hintStyle={hintStyle}
                    />
				)
			}
		</AppBar>
	);
};