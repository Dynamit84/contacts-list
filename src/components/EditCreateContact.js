import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import ContactForm from "./Forms/ContactForm";
import { contactAddRequested, contactEditRequested } from "../actions/actions";
import {clearFields} from 'redux-form';
import PropTypes from 'prop-types';
import makeGetContact from '../selectors/selectors';

class EditCreateContact extends Component {
	
	componentWillReceiveProps(nextProps) {
		if(nextProps.location.pathname.includes('create')) {
			this.props.clearFields('contact', false, false, 'email', 'name', 'phone');
		}
	}
	
    submit = values => {
    	const { contact } = this.props;
    	if(contact) {
			this.props.requestEditContact(values);
		} else {
			const id = Date.now().toString();
			const callsHistory = [];
			this.props.contactAddRequested({...values, id, callsHistory});
		}
    };
	
	render() {
        const { contact } = this.props;
		return (
                <ContactForm pathname={this.props.history.location.pathname} initialValues={contact} onSubmit={this.submit}/>
		);
	}
}

const mapStateToProps = () => {
    const getContact = makeGetContact();

    return (state, ownProps) => {
        return {
            contact: getContact(state, ownProps)
        }
    };
};

const mapDispatchToProps = {
		contactAddRequested: contactAddRequested,
		requestEditContact: contactEditRequested,
        clearFields
};

const propTypes = {
    contact: PropTypes.object,
	contactAddRequested: PropTypes.func.isRequired,
    requestEditContact: PropTypes.func.isRequired,
    clearFields: PropTypes.func.isRequired,
};

EditCreateContact.propTypes = propTypes;

export default connect(mapStateToProps, mapDispatchToProps)(EditCreateContact);