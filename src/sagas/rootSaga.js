import { watchFetchContacts, watchRemoveContact, watchEditContact, watchAddContact } from './sagas';
import { all } from 'redux-saga/effects';

export default function *rootSaga() {
	yield all([
		watchFetchContacts(),
		watchRemoveContact(),
		watchAddContact(),
		watchEditContact()
	])
};