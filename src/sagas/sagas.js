import { call, put, takeLatest } from 'redux-saga/effects';
import { fetchContacts, removeContact, addContact, editContact } from "../api";
import { push } from 'react-router-redux';
import {
	CONTACTS_FETCH_REQUESTED,
	CONTACT_REMOVE_REQUESTED,
	CONTACT_ADD_REQUESTED,
	CONTACT_EDIT_REQUESTED,
} from '../constants';
import {
	contactsFetchSucceeded,
	contactRemoveSucceeded,
    contactAddSucceeded,
	contactEditSucceeded
} from "../actions/actions";


function* getApiData() {
	try {
		const contacts = yield call(fetchContacts);
		yield put(contactsFetchSucceeded(contacts));
	} catch (e) {
		console.log(e);
	}
}

function* removeApiContact(action) {
	try {
		yield call(removeContact, action.payload);
		yield put(contactRemoveSucceeded(action.payload));
		yield put(push('/home'));
	} catch (e) {
		console.log(e);
	}
}

function* addApiContact(action) {
    try {
        yield call(addContact, JSON.stringify(action.payload));
        yield put(contactAddSucceeded(action.payload));
        yield put(push('/home'));
    } catch (e) {
		console.log(e);
    }
}

function* editApiContact(action) {
	try {
		yield call(editContact, JSON.stringify(action.payload), action.payload.id);
		yield put(contactEditSucceeded(action.payload));
		yield put(push('/home'));
	} catch (e) {
		console.log(e);
	}
}

export function* watchFetchContacts() {
	yield takeLatest(CONTACTS_FETCH_REQUESTED, getApiData);
}

export function* watchRemoveContact() {
	yield takeLatest(CONTACT_REMOVE_REQUESTED, removeApiContact);
}

export function* watchAddContact() {
    yield takeLatest(CONTACT_ADD_REQUESTED, addApiContact);
}

export function* watchEditContact() {
	yield takeLatest(CONTACT_EDIT_REQUESTED, editApiContact);
}