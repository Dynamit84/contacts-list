import { createSelector } from 'reselect';

const getContact = (state, ownProps) => state.contacts.find(contact => contact.id === ownProps.match.params.id);

const makeGetContact = () => {
    return createSelector(
        getContact,
        contact => contact
    )
};

export default makeGetContact;