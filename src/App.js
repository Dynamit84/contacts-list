import React, { Component, Fragment } from 'react';
import Home from './components/Home';
import { Route, Switch, withRouter } from 'react-router-dom';
import ContactDetailed from './components/ContactDetailed';
import { contactsFetchRequested } from "./actions/actions";
import { connect } from 'react-redux';
import { compose } from 'recompose';
import EditCreateContact from './components/EditCreateContact';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Header } from './components/Header';
import Settings from "./components/Settings";

class App extends Component {

	componentDidMount() {
		this.props.contactsFetchRequested();
	}
	
	render() {
		return (
			<MuiThemeProvider>
				<Fragment>
					<Header location={this.props.location}/>
					<Switch>
						<Route exact={true} path='/' component={Home} />
						<Route path={'/home'} component={Home} />
						<Route path={'/settings'} component={Settings} />
						<Route path={'/contacts/:id'} component={ContactDetailed} />
						<Route path={'/edit/:id'} component={EditCreateContact} />
						<Route path={'/create'} component={EditCreateContact} />
					</Switch>
				</Fragment>
			</MuiThemeProvider>
		);
	}
}

export default compose(withRouter, connect(null, { contactsFetchRequested }))(App);
