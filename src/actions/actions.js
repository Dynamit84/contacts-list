import { createActions } from 'redux-actions';
import * as constants from '../constants';

export const {
	contactsFetchRequested,
	contactsFetchSucceeded,
	contactRemoveRequested,
	contactRemoveSucceeded,
	contactAddRequested,
	contactAddSucceeded,
	contactEditRequested,
	contactEditSucceeded,
	contactsUpdateSettings
} = createActions(
    constants.CONTACTS_FETCH_REQUESTED,
    constants.CONTACTS_FETCH_SUCCEEDED,
    constants.CONTACT_REMOVE_REQUESTED,
    constants.CONTACT_REMOVE_SUCCEEDED,
    constants.CONTACT_ADD_REQUESTED,
    constants.CONTACT_ADD_SUCCEEDED,
    constants.CONTACT_EDIT_REQUESTED,
    constants.CONTACT_EDIT_SUCCEEDED,
    constants.CONTACTS_UPDATE_SETTINGS,
);
